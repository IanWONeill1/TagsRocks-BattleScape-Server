package script.npc.combat;

import com.palidino.osrs.io.cache.NpcId;
import com.palidino.osrs.model.Hit;
import com.palidino.osrs.model.HitEvent;
import com.palidino.osrs.model.map.MapObject;
import com.palidino.osrs.model.npc.Npc;
import com.palidino.osrs.model.npc.combat.NCombatScript;
import lombok.var;

public class VerzikViturPillarCombat extends NCombatScript {
    private static final int MAP_OBJECT_ID_SUPPORTING_PILLAR = 32687;

    private Npc npc;

    @Override
    public void setNpcHook(Npc npc) {
        this.npc = npc;
    }

    @Override
    public void spawn() {
        npc.getController().addMapObject(new MapObject(MAP_OBJECT_ID_SUPPORTING_PILLAR, npc, 10, 0));
    }

    @Override
    public void despawn() {
        removeMapObject();
        if (npc.getId() == NpcId.COLLAPSING_PILLAR) {
            for (var player : npc.getController().getPlayers()) {
                if (player.isLocked()) {
                    continue;
                }
                player.addHit(new HitEvent(0, player, new Hit(104)));
            }
        }
    }

    @Override
    public void applyDeadStartHook(int deathDelay) {
        removeMapObject();
        npc.setTransformationId(NpcId.COLLAPSING_PILLAR);
        npc.setAnimation(8052);
    }

    private void removeMapObject() {
        npc.getController().removeMapObject(new MapObject(MAP_OBJECT_ID_SUPPORTING_PILLAR, npc, 10, 0));
    }
}

