package script.npc.combat;

import java.util.ArrayList;
import java.util.List;
import com.palidino.osrs.io.cache.NpcId;
import com.palidino.osrs.model.Entity;
import com.palidino.osrs.model.Graphic;
import com.palidino.osrs.model.Hit;
import com.palidino.osrs.model.HitEvent;
import com.palidino.osrs.model.Tile;
import com.palidino.osrs.model.npc.Npc;
import com.palidino.osrs.model.npc.combat.NCombatScript;
import com.palidino.osrs.model.player.Player;
import com.palidino.util.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;

public class VerzikViturCombat extends NCombatScript {
    private static final VerzikPillar[] PILLARS = {
        new VerzikPillar(new Tile(3161, 4318), new Tile[] {
            new Tile(3160, 4318), new Tile(3159, 4317), new Tile(3160, 4317), new Tile(3161, 4317),
            new Tile(3162, 4317), new Tile(3159, 4316), new Tile(3160, 4316)
        }), new VerzikPillar(new Tile(3161, 4312), new Tile[] {
            new Tile(3160, 4313), new Tile(3160, 4312), new Tile(3160, 4311), new Tile(3161, 4311),
            new Tile(3162, 4311), new Tile(3160, 4310), new Tile(3161, 4310)
        }), new VerzikPillar(new Tile(3161, 4306), new Tile[] {
            new Tile(3160, 4306), new Tile(3160, 4305), new Tile(3161, 4305), new Tile(3162, 4305),
            new Tile(3160, 4304), new Tile(3161, 4304)
        }), new VerzikPillar(new Tile(3173, 4318), new Tile[] {
            new Tile(3176, 4318), new Tile(3177, 4317), new Tile(3176, 4317), new Tile(3175, 4317),
            new Tile(3174, 4317), new Tile(3177, 4316), new Tile(3176, 4316)
        }), new VerzikPillar(new Tile(3173, 4312), new Tile[] {
            new Tile(3176, 4313), new Tile(3176, 4312), new Tile(3176, 4311), new Tile(3175, 4311),
            new Tile(3174, 4311), new Tile(3176, 4310), new Tile(3175, 4310)
        }), new VerzikPillar(new Tile(3173, 4306), new Tile[] {
            new Tile(3176, 4306), new Tile(3176, 4305), new Tile(3175, 4305), new Tile(3174, 4305),
            new Tile(3176, 4304), new Tile(3175, 4304)
        })
    };
    private static final int PHASE_1_HIT_DELAY = 16;

    private Npc npc;
    private List<Npc> pillars = new ArrayList<>();

    @Override
    public void setNpcHook(Npc npc) {
        this.npc = npc;
    }

    @Override
    public void spawn() {
        for (var pillar : PILLARS) {
            pillars.add(new Npc(npc.getController(), NpcId.SUPPORTING_PILLAR, pillar.getTile()));
        }
        npc.setHitDelay(PHASE_1_HIT_DELAY);
    }

    @Override
    public void despawn() {
        for (var pillarNpc : pillars) {
            if (pillarNpc.isLocked()) {
                continue;
            }
            npc.getWorld().removeNpc(pillarNpc);
        }
        pillars.clear();
    }

    @Override
    public void tick() {
        if (npc.isLocked()) {
            return;
        }
        if (npc.getId() == NpcId.VERZIK_VITUR_1040_8370) {
            phase1Tick();
        }
    }

    private void phase1Tick() {
        if (npc.getHitDelay() == 2) {
            npc.setAnimation(8109);
            return;
        }
        if (npc.getHitDelay() > 0) {
            return;
        }
        var players = npc.getController().getPlayers();
        var hitEntities = new ArrayList<Entity>();
        for (var player : players) {
            if (player.isLocked()) {
                continue;
            }
            var isProtected = false;
            for (var i = 0; i < PILLARS.length; i++) {
                if (pillars.get(i).isLocked() || !PILLARS[i].isSafe(player)) {
                    continue;
                }
                if (!hitEntities.contains(pillars.get(i))) {
                    hitEntities.add(pillars.get(i));
                }
                isProtected = true;
                break;
            }
            if (isProtected) {
                continue;
            }
            hitEntities.add(player);
        }
        for (var entity : hitEntities) {
            var projectile = Graphic.Projectile.builder().id(1580).entity(entity).startTile(npc)
                    .projectileSpeed(npc.getCombat().getSpeed(null, entity, Hit.Type.MAGIC, 10)).hitType(Hit.Type.MAGIC)
                    .build();
            npc.getCombat().sendMapProjectile(projectile);
            entity.addHit(new HitEvent(projectile.getEventDelay(), entity, npc, new Hit(Utils.randomI(104))));
            if (entity instanceof Player) {
                entity.setGraphic(new Graphic(1581, 0, projectile.getContactDelay()));
            } else {
                entity.setGraphic(new Graphic(1582, 0, projectile.getContactDelay()));
            }
        }
        npc.setHitDelay(PHASE_1_HIT_DELAY);
    }
}


@AllArgsConstructor
@Getter
class VerzikPillar {
    private Tile tile;
    private Tile[] safeTiles;

    public boolean isSafe(Tile tile) {
        for (var safeTile : safeTiles) {
            if (!tile.matchesTile(safeTile)) {
                continue;
            }
            return true;
        }
        return false;
    }
}
